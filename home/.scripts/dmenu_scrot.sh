#!/usr/bin/env bash
#
#  TODO:
#   use xmp instead of scrot

IMG_PATH=$HOME/Pictures/dumps/
UL="pb 0x0"
TIME=3000 #Seconds notification should remain visible
FILENAME=$(date +%Y-%m-%d_@_%H-%M-%S-scrot.png)

prog="1.Fullscreen
2.Delayed
3.Section
4.Window
5.Upload
6.Delayed_upload
7.Section_upload"

cmd=$(dmenu -l 20  -fn InputMonoCondensed:style=Regular:pixelsize=14:antialias=true -nb "#1b1b1b" -nf "#f2f2f2" -sb "#84998f" -sf "#000000" -p 'Choose Screenshot Type'   <<< "$prog")

cd $IMG_PATH
case ${cmd%% *} in

	1.Fullscreen)	scrot -d 1 $FILENAME && notify-send -u low -t $TIME 'Scrot' 'Fullscreen taken and saved' ;;
	2.Delayed) scrot -d 4 $FILENAME && notify-send -u low -t $TIME 'Scrot' 'Fullscreen Screenshot saved' ;;
	3.Section)	scrot -s $FILENAME && notify-send -u low -t $TIME 'Scrot' 'Screenshot of section saved' ;;
    4.Window) scrot -ub -d 1 $FILENAME && notify-send -i none -u low -t $TIME 'scrot' 'Window Screenshot saved' ;;
    5.Upload) scrot -d 1 $FILENAME && $UL $FILENAME | xclip -selection clipboard && notify-send -u low -t $TIME "Screenshot" "Uploaded to $(xclip -o;echo)" ;;
    6.Delayed_upload) scrot -d 4 $FILENAME && $UL $FILENAME | xclip -selection clipboard && notify-send -u low -t $TIME "Sreenshot" "Uploaded to $(xclip -o;echo)" ;;
    7.Section_upload) scrot -s $FILENAME && $UL $FILENAME | xclip -selection clipboard && notify-send -u low -t $TIME "Screenshot" "Uploaded to $(xclip -o;echo)";;

  	*)		exec "'${cmd}'" ;;
esac
