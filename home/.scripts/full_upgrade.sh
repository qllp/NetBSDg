#!/bin/sh

DIR1=$HOME/pkgsrc/
DIR2=$HOME/pkgsrc/wip/
DIR3=$HOME/pkgsrc/pkgtools/osabi
DIR4=$HOME/pkgsrc/pkgtools/x11-links

# remove osabi and x11-links before upgrade
please pkg_delete osabi-NetBSD-9.2_STABLE x11-links-1.34

# updade pkgsrc
cd $DIR1 && please cvs update -dP

# update pkgsrc/wip
cd $DIR2 && git pull -r

# update repositories
please pkgin update

# upgrade packages
please pkgin -y full-upgrade

# remove orphan packlages and clean pkgin
please pkgin -y autoremove && please pkgin clean

# build and install osabi and x11-links
cd $DIR3 && make -s
please make install clean distclean clean-depends

cd $DIR4 && make -s
please make install clean distclean clean-depends

exit
