#!/bin/sh

DIR1=$HOME/pkgsrc/pkgtools/osabi
DIR2=$HOME/pkgsrc/pkgtools/x11-links

# remove the old packages
please pkg_delete osabi-NetBSD-9.2_STABLE x11-links-1.34

# build and install osabi
cd $DIR1 && make -s
please make install clean distclean clean-depends

#build and install x11-links
cd $DIR2 && make -s
please make install clean distclean clean-depends

exit
