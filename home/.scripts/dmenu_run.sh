#!/bin/sh
dmenu_run -fn InputMonoCondensed:style=Regular:pixelsize=14:antialias=true -nb "#1b1b1b" -nf "#f2f2f2" -sb "#84998f" -sf "#000000"
