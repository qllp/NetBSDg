#!/bin/sh

set -eu
ffmpeg4 -thread_queue_size 1024 \
        -f s16le -ar 44100 -ac 2 -i /dev/pad0 \
        -f oss -i /dev/audio \
        -filter_complex '[0] [1] amix' \
        -f x11grab -r 30 -s 1366x768 -i :0.0 -vcodec libx264 -preset ultrafast \
	"$1"	
