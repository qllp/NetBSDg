(function() {
    if (window.location.hostname != 'https://old.reddit.com')
        return;

    var selectors = ['div#sub-header', 'div.sidebar-collapsed-menu'];
    for (var selector_i = 0; selector_i < selectors.length; selector_i++)
    {
        var selector = selectors[selector_i];
        var elements = document.body.querySelectorAll(selector);
        for (var elem_i = 0; elem_i < elements.length; elem_i++)
        {
            var elem = elements[elem_i];
            elem.parentNode.removeChild(elem);
        }
    }
}());
