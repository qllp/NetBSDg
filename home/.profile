# .profile

# Set text editor
export EDITOR=vim

# Set configuration tool for Qt5-applications
#export QT_QPA_PLATFORMTHEME=qt5ct

# Set the search path for programs.
#export HOME=/usr/home/dave
PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R7/bin:/usr/pkg/bin
PATH=${PATH}:/usr/pkg/sbin:/usr/games:/usr/local/bin:/usr/local/sbin:/home/dave/.cargo/bin:/home/dave/.local/bin
export PATH

# Configure the shell to load .shrc at startup time.
# This will happen for every shell started, not just login shells.
export ENV=$HOME/.shrc

# ksh history
export HISTFILE=$HOME/.ksh_history
export HISTSIZE=10000

# shfm opener
export SHFM_OPENER="/home/dave/.open.sh"

# Hairloom-mailx
export NAILRC=$HOME/.nailrc

# UTF-8 Locale setting
export LANG=en_AU.UTF-8
export LC_CTYPE="$LANG"
export LC_ALL=""

# gpg
GPG_TTY=$(tty)
export GPG_TTY

# lariza
export LARIZA_ACCEPTED_LANGUAGE=en_AU
export LARIZA_DOWNLOAD_DIR=$HOME/Downloads
export LARIZA_HOME_URI=file:///home/dave/.startpage/index.html
