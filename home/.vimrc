"
"   ░█░█░▀█▀░█▄█░█▀▄░█▀▀
"   ░▀▄▀░░█░░█░█░█▀▄░█░░
"   ░░▀░░▀▀▀░▀░▀░▀░▀░▀▀▀
"
" vim-plug
call plug#begin('~/.vim/plugged')
Plug 'mhinz/vim-startify'
Plug 'junegunn/goyo.vim'

" All of your Plugins must be added before the following line
call plug#end()            " required

" Brief help
" :PlugList       - lists configured plugins
" :PlugInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PlugSearch foo - searches for foo; append `!` to refresh local cache
" :PlugClean      - confirms removal of unused plugins; append `!` to auto-approve removal

" general settings
syntax on
colors garbage-oracle
set t_Co=256
set nocompatible
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
"set autoindent
set smarttab
set complete-=i
set backspace=indent,eol,start
set history=50
set ruler
set scrolloff=1
set sidescrolloff=5
set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
set wildmenu
set cursorline
set showmatch
set number
set relativenumber
"set colorcolumn=75

" dont automatically add comments on new lines
autocmd BufNewFile,BufRead * setlocal formatoptions-=ro

" color options
highlight LineNr ctermfg=DarkGrey
highlight LineNr ctermbg=NONE
highlight CursorLine ctermbg=YELLOW
highlight CursorLineNr cterm=NONE
highlight CursorLineNr ctermbg=YELLOW
highlight CursorLineNr ctermfg=130
highlight ColorColumn ctermbg=249

" status line
" to get a list of available colours run
" :so $VIMRUNTIME/syntax/hitest.vim
set laststatus=2			    "show statusbar
set statusline=				    "left side
set statusline+=%#Search#		"colour
set statusline+=\ %y			"filetype
set statusline+=\ %r			"readonly flag
set statusline+=%#StatusLine#	"colour
set statusline+=\ %F			"full path to file
set statusline+=%=			    "right side
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}  "utf-8
set statusline+=\ [%{&fileformat}\] "[unix]
set statusline+=\ %#Search#         "colour
set statusline+=\ %l/%L			"line numbers
set statusline+=\ [%c]			"column number

" autoclose ftw
inoremap " ""<left>
inoremap ' ''<left>
inoremap ` ``<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>)0
inoremap {;<CR> {<CR>};<ESC>0

"Goyo settings
function! s:goyo_enter()
    set noshowmode
    set noshowcmd
    set nocursorline
endfunction

function! s:goyo_leave()
    set showmode
    set showcmd
    set cursorline
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

" Remove trailing white spaces
autocmd BufWritePre * %s/\s\+$//e

"config startify plugin
"
let g:startify_custom_header = [
\ '                                 ',
\ '      ⢀⣀ ⣰⡀ ⢀⣀ ⡀⣀ ⣰⡀ ⣀⡀ ⢀⣀ ⢀⡀ ⢀⡀ ',
\ '      ⠭⠕ ⠘⠤ ⠣⠼ ⠏  ⠘⠤ ⡧⠜ ⠣⠼ ⣑⡺ ⠣⠭ ',
\ ]


let g:startify_lists = [
\ { 'type': 'files',    'header': ['    last modified:'] },
\ { 'type': 'bookmarks', 'header': ['    bookmarks:'] },
\ { 'type': 'dir',  'header': ['    files in folder '. getcwd()]},
\ ]

let g:startify_bookmarks = [ {'v': '~/.vimrc'}, {'s': '~/.shrc'}, {'x': '~/.xinitrc'}, {'w': '~/.config/sdorfehs/config'}, {'p': '~/.profile'}, {'X': '~/.Xresources'}]
