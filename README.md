# NetBSD
A personal repository of my NetBSD dotfiles. Most of the configs have been borrowed from other, more experienced users such as [pin](https://codeberg.org/pin/) & [jcs](https://github.com/jcs)

![alt text](https://i.postimg.cc/NtPXGSMx/az7qiggpv9n71.png)
